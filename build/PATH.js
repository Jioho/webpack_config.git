const path = require('path')

const ROOT = path.resolve(__dirname, '../')

const PATH = {
  ROOT: ROOT,
  DIST: path.resolve(ROOT, 'dist'),
  tmp_main: path.resolve(ROOT, 'src/main.js'),
  TEMPLATE_HTML: path.resolve(ROOT, 'public/index.html'),
  BASE_CONFIG: path.resolve(ROOT, 'build/webpack.base.js'),
  DEV_CONFIG: path.resolve(ROOT, 'build/webpack.dev.js'),
  PROD_CONFIG: path.resolve(ROOT, 'build/webpack.prod.js')
}

module.exports = PATH
