const express = require('express')
const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const { DEV_CONFIG } = require('./PATH')
const net = require('net')

const app = express()
const config = require(DEV_CONFIG)

const compiler = webpack(config)

const devMiddleware = webpackDevMiddleware(compiler, {
  publicPath: config.output.publicPath,
  quiet: true,
  reload: true,
  hot: true // 开启热更新，浏览器自动刷新
  // inline: true
})

// 告知 express 使用 webpack-dev-middleware，
// 以及将 webpack.config.js 配置文件作为基础配置。
app.use(devMiddleware)

// 热更新
app.use(
  webpackHotMiddleware(compiler, {
    log: () => console.log,
    reload: true
  })
)

// setTimeout(() => {
//   app.listen('8080', function() {
//     console.log('> Listening at 8080 !\n')
//   })
// }, 2000)

app.listen(8080, function() {
  console.log('> Listening at 8080 !\n')
})
