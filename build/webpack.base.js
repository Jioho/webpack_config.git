const HtmlWebpackPlugins = require('html-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const { DIST, tmp_main, TEMPLATE_HTML } = require('./PATH')

const devMode = process.env.NODE_ENV === 'development' // 是否是开发模式

module.exports = {
  // entry: { index: devMode ? ['webpack-hot-middleware/client', tmp_main] : [tmp_main] },
  entry: {
    goods: devMode ? ['webpack-hot-middleware/client', './src/goods/index.js'] : ['./src/goods/index.js'],
    user: devMode ? ['webpack-hot-middleware/client', './src/user/index.js'] : ['./src/user/index.js'],
    user_setting: devMode
      ? ['webpack-hot-middleware/client', './src/user/setting/index.js']
      : ['./src/user/setting/index.js']
  },
  output: {
    filename: 'js/[name].[contenthash].js',
    publicPath: './', // base.js 中的这个配置其实会被后面覆盖掉，所以写哪个都没关系
    path: DIST
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: ['vue-loader']
      },
      {
        test: /\.(less|css)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: '../',
              // only enable hot in development
              hmr: devMode,
              // if hmr does not work, this is a forceful method.
              reloadAll: devMode
            }
          },
          { loader: 'css-loader', options: { esModule: false } },
          'postcss-loader',
          'less-loader'
        ]
      },
      // 图片文件处理
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 5000,
            esModule: false,
            outputPath: 'assets/images',
            name: '[name].[contenthash:4].[ext]'
          }
        }
      },
      // 音频文件
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 5000,
            esModule: false,
            outputPath: 'assets/audio',
            name: '[name].[contenthash:4].[ext]'
          }
        }
      },
      // 字体文件
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            esModule: false,
            outputPath: 'assets/font',
            name: '[name].[contenthash:4].[ext]'
          }
        }
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-transform-runtime']
          }
        }
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: 'css/[name].[contenthash].css'
    }),
    // // 设置html模板生成路径
    // new HtmlWebpackPlugins({
    //   filename: 'index.html',
    //   template: TEMPLATE_HTML,
    //   chunks: ['main'] // 指定在html自动引入的js打包文件
    // })
    new HtmlWebpackPlugins({
      filename: 'goods.html',
      template: TEMPLATE_HTML,
      chunks: ['goods']
    }),
    new HtmlWebpackPlugins({
      filename: 'user.html',
      template: TEMPLATE_HTML,
      chunks: ['user']
    }),
    new HtmlWebpackPlugins({
      filename: 'user/setting.html',
      template: TEMPLATE_HTML,
      publicPath: '../',
      chunks: ['user_setting']
    })
  ]
}
