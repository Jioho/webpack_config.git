const { merge } = require('webpack-merge')
const { DIST, BASE_CONFIG } = require('./PATH')
const baseConfig = require(BASE_CONFIG)
const webpack = require('webpack')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = merge(baseConfig, {
  mode: 'development',
  output: {
    filename: 'js/[name].[hash].js',
    publicPath: '/',
    path: DIST
  },
  devServer: {
    contentBase: DIST //指定需要提供给本地服务的内容的路径，默认加载index.html文件，可根据需要修改
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new MiniCssExtractPlugin({
      filename: 'css/[name].css'
    })
  ]
})
