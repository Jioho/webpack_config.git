const { merge } = require('webpack-merge')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const { DIST, BASE_CONFIG } = require('./PATH')
const baseConfig = require(BASE_CONFIG)

module.exports = merge(baseConfig, {
  mode: 'production',
  output: {
    filename: 'js/[name].[contenthash].js',
    publicPath: './',
    path: DIST
  },
  plugins: [new CleanWebpackPlugin()]
})
