import Vue from 'vue'
import App from './setting.vue'

new Vue({
  el: '#app',
  render: h => h(App)
})
